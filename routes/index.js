const express = require('express');
const web3 = require('web3');
const axios = require('axios');
const router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', {title: 'Express'});
});

// Il doit récupérer le prix entre les deux tokens.
// Il doit récupérer le prix du tokenA en USD.
// Il doit récupérer le prix du tokenB en USD.
// Il doit stocker les informations de prix dans le contrat Oracle.
router.get('/api/:tokenA-:tokenB', async (req, res, next) => {
        const {tokenA, tokenB} = req.params;
        try {
            const responseA = await axios.get(`https://api.coingecko.com/api/v3/simple/price?ids=${tokenA}&vs_currencies=usd`);
            const responseB = await axios.get(`https://api.coingecko.com/api/v3/simple/price?ids=${tokenB}&vs_currencies=usd`);
            const priceAUSD = responseA.data[tokenA.toLowerCase()].usd;
            const priceBUSD = responseB.data[tokenB.toLowerCase()].usd;

            // Déclaration de l'address du contrat Oracle
            const oracleAddress = "" // "0x5FbDB2315678afecb367f032d93F642f64180aa3";
            // Déclaration de l'abi du contrat Oracle
            const oracleAbi = [];
            // Déclaration de l'instance du contrat Oracle
            const oracle = new web3.eth.Contract(oracleAbi, oracleAddress);
            // Stockage des informations de prix dans le contrat Oracle (à faire)
            const tx = await oracle.methods.update(priceAUSD, priceBUSD).send({from: "0x5FbDB2315678afecb367f032d93F642f64180aa3"});
            console.log(tx);
            res.status(200).json({data: {priceAUSD, priceBUSD}});
        } catch (error) {
            next(error);
        }
    }
);

module.exports = router;
